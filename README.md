
### Workshop on Oscilloscopes ###

Files for the workshop on Oscilloscopes, held at the [Department of Electronics, Telecommunications and Informatics](http://www.ua.pt/deti) in the University of Aveiro.

Repo maintained by [Pedro Fonseca](http://sweet.ua.pt/pf). 