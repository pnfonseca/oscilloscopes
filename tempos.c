/*
* multint.c
* 
* Timing operations on integers
* 
*/

#include <detpic32.h>

#define TEST_PIN  PORTDbits.RD3	// OC4

int
soma (int x, int y)
{
  int z;
  z = x + y;

  printf("%d\n",z);

  return (z);
}

int
main (void)
{
  long x, y, z;

  TRISDbits.TRISD3 = 0;

  x = 3;
  y = 4;

  while (1)
    {
      x++;

      TEST_PIN = 1;

      z = x + y;

      // z = soma(x,y);

      TEST_PIN = 0;

    }
}
